<?php
class Template
{
    
    public function __construct()
    {
        $this->ci =& get_instance();
            
    }
    
    public function render($path, $data)
    {
       $data['head'] = $this->ci->load->view('layout/head', $data, TRUE);
       $data['_navbar'] = $this->ci->load->view('layout/_navbar', $data, TRUE);
       $data['_sidebar'] = $this->ci->load->view('layout/_sidebar', $data, TRUE);
       
       $data['_page'] = $this->ci->load->view('page/' . $data['page'], $data, TRUE);

       $data['script'] = $this->ci->load->view('layout/script', $data, TRUE);

       $this->ci->load->view('layout/template', $data);
    }
}

?>