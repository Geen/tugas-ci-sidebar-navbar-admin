<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo $head ?>
    <title> <?php echo $page ?> </title>
</head>
<body>
    <?php echo $_navbar ?>
    <div class="container-fluid page-body-wrapper">
        <?php echo $_sidebar ?>
        <?php echo $_page?>
    </div>        
    <?php echo $script ?>
</body>
</html>