<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index()
    {
        $data['page'] = "dashboard";
        $this->template->render('layout/template', $data);
    }
    public function page()
    {
        $data['page'] = $this->uri->segment(3);
        
        $this->template->render('layout/template', $data);
    }
}

/* End of file Home.php */

?>